# Task manager #

This is a super simple task manager microservice.
The app exposes an API which can be used to perform actions such as: adding processes, listing processes and killing processes.

## Requirements ##

* Java 17
* Maven 3.x
* Postman to use postman collection

## How to run ##

The application can be started from the commandline with the following maven command:

````
mvn spring-boot:run
````

The application will start on port 8080.

## Configuration ##

The app has very simple configuration. 
The configurable values determine which processmanager should be active and the max number of processes allowed.

````
process:
  manager: fifo
  capacity:
    max: 10
````
Allowed values for process.manager are "fifo", "maxcapacity" and "priority".

## API ##

The application exposes a REST API that consumes and produces JSON. Every endpoint returns the current state of the
process list.

### Endpoints ###

---
**Request:**
````
POST /api/process/add
````
POST a process to the task manager.

**Request body example:**
````
{
    "priority":"1"
}
````
Priority is the priority of the process, valid values are in the range 1 < x > 3.

---
**Request:**
````
GET /api/process/{sorted}
````
GET a sorted list of processes. Valid sort values are "priority" and "timestamp".

---
**Request:**
````
GET /api/process/kill/{id}
````
Kill the process with the supplied id if it exists in the task manager.

---
**Request:**
````
GET /api/process/killgroup/{priority}
````
Kill all the process with the supplied priority if they exist in the task manager.

---
**Request:**
````
GET /api/process/killall
````
Kill all the process in the task manager.

**Response example:**
````
{
    "processList": [
        {
            "pid": "4e5817c7-ab77-4e4c-b9f1-e9e9b2f3c41b",
            "priority": 1,
            "timeStamp": "2022-03-08T18:54:15.446+00:00"
        },
        {
            "pid": "3a021b90-cc5d-42e9-a36f-aa71b118c3c6",
            "priority": 3,
            "timeStamp": "2022-03-08T18:54:11.423+00:00"
        },
        {
            "pid": "3bc09549-cd1d-4f29-a529-cbc26ef24b8d",
            "priority": 3,
            "timeStamp": "2022-03-08T18:54:10.931+00:00"
        }
    ],
    "processCount": 3
}
````