package alex.assignment.iptiq.model;

import java.util.Date;
import java.util.UUID;

public class Process {

    private final UUID pid;
    private final int priority;
    private final Date timeStamp;

    public Process(UUID pid,
                   int priority,
                   Date timeStamp) {
        this.pid = pid;
        this.priority = priority;
        this.timeStamp = timeStamp;
    }

    public UUID getPid() {
        return pid;
    }

    public int getPriority() {
        return priority;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }
}
