package alex.assignment.iptiq.api;

import alex.assignment.iptiq.model.Process;

import java.util.List;

public class TaskResponse {

    private final List<Process> processList;
    private final long processCount;

    public TaskResponse(List<Process> processList) {
        this.processList = processList;
        this.processCount = processList.size();
    }

    public List<Process> getProcessList() {
        return processList;
    }

    public long getProcessCount() {
        return processCount;
    }
}
