package alex.assignment.iptiq.api.request;

import com.fasterxml.jackson.annotation.JsonCreator;

public class TaskRequest {

    private final int priority;

    @JsonCreator
    public TaskRequest(int priority) {
        this.priority = priority;
    }

    public int getPriority() {
        return priority;
    }
}
