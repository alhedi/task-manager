package alex.assignment.iptiq.api.exception;

import java.util.UUID;

public class TaskExceptionResponse {

    private final UUID exceptionId;
    private final String message;

    public TaskExceptionResponse(UUID exceptionId,
                                 String message) {
        this.exceptionId = exceptionId;
        this.message = message;
    }

    public UUID getExceptionId() {
        return exceptionId;
    }

    public String getMessage() {
        return message;
    }
}
