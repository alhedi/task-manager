package alex.assignment.iptiq.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.UUID;

@ControllerAdvice
public class TaskControllerAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = TaskException.class)
    protected ResponseEntity<TaskExceptionResponse> handlInvalidTaskRequestException(TaskException exception,
                                                                                     WebRequest request) {
        TaskExceptionResponse taskExceptionResponse = new TaskExceptionResponse(UUID.randomUUID(), exception.getReason());
        return new ResponseEntity<>(taskExceptionResponse, HttpStatus.BAD_REQUEST);
    }
}
