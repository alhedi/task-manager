package alex.assignment.iptiq.api.exception;

public class TaskException extends Throwable {

    private final String reason;

    public TaskException(String reason) {
        this.reason = reason;
    }

    public String getReason() {
        return reason;
    }
}
