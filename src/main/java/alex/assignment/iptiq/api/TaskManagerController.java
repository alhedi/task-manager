package alex.assignment.iptiq.api;

import alex.assignment.iptiq.api.exception.TaskException;
import alex.assignment.iptiq.api.exception.TaskExceptionResponse;
import alex.assignment.iptiq.api.request.TaskRequest;
import alex.assignment.iptiq.service.ProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("api")
public class TaskManagerController {

    @Autowired
    private ProcessService taskService;

    @PostMapping(value = "process/add", produces = "application/json", consumes = "application/json")
    public TaskResponse addProcess(@RequestBody TaskRequest taskRequest) throws TaskException {
        return taskService.addProcess(taskRequest);
    }

    @GetMapping(value = "process/{sorted}", produces = "application/json")
    public TaskResponse getList(@PathVariable String sorted) {
        return taskService.getSorted(sorted);
    }

    @GetMapping(value = "process/kill/{id}", produces = "application/json", consumes = "application/json")
    public TaskResponse killProcess(@PathVariable UUID id) {
        return taskService.killProcess(id);
    }

    @GetMapping(value = "process/killgroup/{priority}", produces = "application/json", consumes = "application/json")
    public TaskResponse killProcess(@PathVariable int priority) {
        return taskService.killProcesses(priority);
    }

    @GetMapping(value = "process/killall", produces = "application/json", consumes = "application/json")
    public TaskResponse killAll() {
        return taskService.killAll();
    }
}
