package alex.assignment.iptiq.service;

import alex.assignment.iptiq.api.exception.TaskException;
import alex.assignment.iptiq.api.exception.TaskExceptionResponse;
import alex.assignment.iptiq.api.request.TaskRequest;
import alex.assignment.iptiq.api.TaskResponse;
import alex.assignment.iptiq.manager.ProcessManager;
import alex.assignment.iptiq.model.Process;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.UUID;

@Service
public class ProcessService {

    private final ProcessManager processManager;

    @Autowired
    public ProcessService(ProcessManager processManager) {
        this.processManager = processManager;
    }

    public TaskResponse addProcess(TaskRequest taskRequest) throws TaskException {
        if (taskRequest.getPriority() < 1 ||taskRequest.getPriority() > 3) {
            throw new TaskException("Priority outside of valid values. Valid values are [1, 2, 3]");
        }
        return new TaskResponse(processManager.addProcess(taskRequest));
    }

    public TaskResponse getSorted(String sorted) {
        String sort = sorted == null ? "" : sorted;

        switch (sort) {
            case "priority" -> processManager.getProcesses().sort(Comparator.comparing(Process::getPriority));
            case "timestamp" -> processManager.getProcesses().sort(Comparator.comparing(Process::getTimeStamp));
        }

        return new TaskResponse(processManager.getProcesses());
    }

    public TaskResponse killProcess(UUID pid) {
        return new TaskResponse(processManager.killProcess(pid));
    }

    public TaskResponse killProcesses(int priority) {
        return new TaskResponse(processManager.killProcesses(priority));
    }

    public TaskResponse killAll() {
        return new TaskResponse(processManager.killAll());
    }
}
