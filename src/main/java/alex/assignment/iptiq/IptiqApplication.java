package alex.assignment.iptiq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IptiqApplication {

	public static void main(String[] args) {
		SpringApplication.run(IptiqApplication.class, args);
	}

}
