package alex.assignment.iptiq.manager;

import alex.assignment.iptiq.api.request.TaskRequest;
import alex.assignment.iptiq.api.TaskResponse;
import alex.assignment.iptiq.model.Process;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Component
@ConditionalOnProperty(
        value = "process.manager",
        havingValue = "maxcapacity",
        matchIfMissing = false
)
public class MaxCapacityProcessManager extends AbstractProcessManager {

    private final long maxAmount;

    public MaxCapacityProcessManager(@Value("${process.capacity.max}") long maxAmount) {
        super();
        this.maxAmount = maxAmount;
    }

    @Override
    public List<Process> addProcess(TaskRequest taskRequest) {
        if (processes.size() < maxAmount) {
            processes.add(new Process(UUID.randomUUID(), taskRequest.getPriority(), new Date()));
        }

        return processes;
    }

}
