package alex.assignment.iptiq.manager;

import alex.assignment.iptiq.model.Process;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public abstract class AbstractProcessManager implements ProcessManager {

    protected LinkedList<Process> processes;

    protected AbstractProcessManager() {
        this.processes = new LinkedList<>();
    }

    @Override
    public List<Process> getProcesses() {
        return processes;
    }

    @Override
    public List<Process> killProcess(UUID pid) {
        processes.removeIf(process -> process.getPid().equals(pid));
        return processes;
    }

    @Override
    public List<Process> killProcesses(int priority) {
        processes.removeIf(process -> process.getPriority() == priority);
        return processes;
    }

    @Override
    public List<Process> killAll() {
        processes.clear();
        return processes;
    }
}
