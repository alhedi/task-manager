package alex.assignment.iptiq.manager;

import alex.assignment.iptiq.api.request.TaskRequest;
import alex.assignment.iptiq.model.Process;

import java.util.List;
import java.util.UUID;

public interface ProcessManager {

    List<Process> addProcess(TaskRequest taskRequest);
    List<Process> getProcesses();
    List<Process> killProcess(UUID pid);
    List<Process> killProcesses(int priority);
    List<Process> killAll();
}
