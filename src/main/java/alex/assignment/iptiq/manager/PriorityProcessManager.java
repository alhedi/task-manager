package alex.assignment.iptiq.manager;

import alex.assignment.iptiq.api.request.TaskRequest;
import alex.assignment.iptiq.api.TaskResponse;
import alex.assignment.iptiq.model.Process;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
@ConditionalOnProperty(
        value = "process.manager",
        havingValue = "priority",
        matchIfMissing = false
)
public class PriorityProcessManager extends AbstractProcessManager {

    private final long maxAmount;

    public PriorityProcessManager(@Value("${process.capacity.max}") long maxAmount) {
        super();
        this.maxAmount = maxAmount;
    }

    @Override
    public List<Process> addProcess(TaskRequest taskRequest) {
        if (processes.size() >= maxAmount) {
            findLessPrioritized(taskRequest)
                    .ifPresent(process -> {
                        processes.remove(process);
                        processes.push(new Process(UUID.randomUUID(), taskRequest.getPriority(), new Date()));
                    });
        } else {
            processes.push(new Process(UUID.randomUUID(), taskRequest.getPriority(), new Date()));
        }


        return processes;
    }

    private Optional<Process> findLessPrioritized(TaskRequest taskRequest) {
        return processes.stream()
                .filter(process -> process.getPriority() > taskRequest.getPriority())
                .min(Comparator.comparing(Process::getTimeStamp));
    }

}
