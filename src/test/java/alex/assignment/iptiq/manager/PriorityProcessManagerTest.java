package alex.assignment.iptiq.manager;

import alex.assignment.iptiq.api.request.TaskRequest;
import alex.assignment.iptiq.model.Process;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

class PriorityProcessManagerTest {

    private PriorityProcessManager priorityProcessManager;

    @BeforeEach
    public void beforeEach() {
        priorityProcessManager = new PriorityProcessManager(1);
    }

    @Test
    public void addProcessTest() {
        List<Process> processes = priorityProcessManager.addProcess(new TaskRequest(1));

        Assertions.assertNotNull(processes);
        Assertions.assertFalse(processes.isEmpty());
        Assertions.assertEquals(1, processes.size());
        Assertions.assertEquals(1, processes.get(0).getPriority());
    }

    @Test
    public void addProcessBeyondCapacityPriorityTest() {
        List<Process> processes = priorityProcessManager.addProcess(new TaskRequest(2));

        Assertions.assertNotNull(processes);
        Assertions.assertFalse(processes.isEmpty());
        Assertions.assertEquals(1, processes.size());
        Assertions.assertEquals(2, processes.get(0).getPriority());

        processes = priorityProcessManager.addProcess(new TaskRequest(1));

        Assertions.assertNotNull(processes);
        Assertions.assertFalse(processes.isEmpty());
        Assertions.assertEquals(1, processes.size());
        Assertions.assertEquals(1, processes.get(0).getPriority());
    }

    @Test
    public void addProcessBeyondCapacityDateTest() {
        priorityProcessManager = new PriorityProcessManager(2);
        List<Process> processes = priorityProcessManager.addProcess(new TaskRequest(3));

        Assertions.assertNotNull(processes);
        Assertions.assertFalse(processes.isEmpty());
        Assertions.assertEquals(1, processes.size());
        Assertions.assertEquals(3, processes.get(0).getPriority());

        Process oldProcess = processes.get(0);
        Optional<Process> newProcess = priorityProcessManager.addProcess(new TaskRequest(3)).stream()
                .filter(process -> process.getTimeStamp().after(oldProcess.getTimeStamp()))
                .findFirst();

        Assertions.assertTrue(newProcess.isPresent());

        processes = priorityProcessManager.addProcess(new TaskRequest(2));

        Assertions.assertNotNull(processes);
        Assertions.assertFalse(processes.isEmpty());
        Assertions.assertEquals(2, processes.size());
        Assertions.assertTrue(processes.contains(newProcess.get()));
        Assertions.assertFalse(processes.contains(oldProcess));
        Assertions.assertNotEquals(oldProcess, newProcess.get());
        Assertions.assertTrue(oldProcess.getTimeStamp().before(newProcess.get().getTimeStamp()));
    }


}