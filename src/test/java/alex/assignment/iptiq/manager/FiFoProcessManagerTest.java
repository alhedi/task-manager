package alex.assignment.iptiq.manager;

import alex.assignment.iptiq.api.request.TaskRequest;
import alex.assignment.iptiq.model.Process;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

class FiFoProcessManagerTest {

    private FiFoProcessManager fiFoProcessManager;

    @BeforeEach
    public void beforeEach() {
        fiFoProcessManager = new FiFoProcessManager(1);
    }

    @Test
    public void addProcessTest() {
        List<Process> processes = fiFoProcessManager.addProcess(new TaskRequest(1));

        Assertions.assertNotNull(processes);
        Assertions.assertFalse(processes.isEmpty());
        Assertions.assertEquals(1, processes.size());
        Assertions.assertEquals(1, processes.get(0).getPriority());
    }

    @Test
    public void addProcessBeyondCapacityTest() {
        List<Process> processes = fiFoProcessManager.addProcess(new TaskRequest(1));

        Assertions.assertNotNull(processes);
        Assertions.assertFalse(processes.isEmpty());
        Assertions.assertEquals(1, processes.size());
        Assertions.assertEquals(1, processes.get(0).getPriority());

        processes = fiFoProcessManager.addProcess(new TaskRequest(2));

        Assertions.assertNotNull(processes);
        Assertions.assertFalse(processes.isEmpty());
        Assertions.assertEquals(1, processes.size());
        Assertions.assertEquals(2, processes.get(0).getPriority());
    }

}