package alex.assignment.iptiq.manager;

import alex.assignment.iptiq.api.request.TaskRequest;
import alex.assignment.iptiq.model.Process;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

class MaxCapacityProcessManagerTest {

    private MaxCapacityProcessManager maxCapacityProcessManager;

    @BeforeEach
    public void beforeEach() {
        maxCapacityProcessManager = new MaxCapacityProcessManager(1);
    }

    @Test
    public void addProcessTest() {
        List<Process> processes = maxCapacityProcessManager.addProcess(new TaskRequest(1));

        Assertions.assertNotNull(processes);
        Assertions.assertFalse(processes.isEmpty());
        Assertions.assertEquals(1, processes.size());
        Assertions.assertEquals(1, processes.get(0).getPriority());
    }

    @Test
    public void addProcessBeyondCapacityTest() {
        List<Process> processes = maxCapacityProcessManager.addProcess(new TaskRequest(1));

        Assertions.assertNotNull(processes);
        Assertions.assertFalse(processes.isEmpty());
        Assertions.assertEquals(1, processes.size());
        Assertions.assertEquals(1, processes.get(0).getPriority());

        processes = maxCapacityProcessManager.addProcess(new TaskRequest(2));

        Assertions.assertNotNull(processes);
        Assertions.assertFalse(processes.isEmpty());
        Assertions.assertEquals(1, processes.size());
        Assertions.assertEquals(1, processes.get(0).getPriority());
    }

}