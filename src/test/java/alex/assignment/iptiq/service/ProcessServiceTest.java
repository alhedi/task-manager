package alex.assignment.iptiq.service;

import alex.assignment.iptiq.api.TaskResponse;
import alex.assignment.iptiq.manager.ProcessManager;
import alex.assignment.iptiq.model.Process;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.*;

@SpringBootTest(classes = ProcessService.class)
class ProcessServiceTest {

    @MockBean
    private ProcessManager processManager;

    private List<Process> processes;

    @Autowired
    private ProcessService processService;

    @BeforeEach
    public void beforeEach() {
        MockitoAnnotations.openMocks(this);
        processes = new LinkedList<>();
    }

    @Test
    public void getSortedPriorityTest() {
        processes.add(new Process(UUID.randomUUID(), 2, new Date()));
        processes.add(new Process(UUID.randomUUID(), 1, new Date()));
        processes.add(new Process(UUID.randomUUID(), 3, new Date()));

        Mockito.when(processManager.getProcesses()).thenReturn(processes);

        TaskResponse sortedProcesses = processService.getSorted("priority");

        Assertions.assertNotNull(sortedProcesses);

        for (int i = 1; i < 4; ++i) {
            Assertions.assertEquals(i, sortedProcesses.getProcessList().get(i-1).getPriority());
        }
    }

    @Test
    public void getSortedTimestampTest() {
        processes.add(new Process(UUID.randomUUID(), 2, new Date()));
        processes.add(new Process(UUID.randomUUID(), 1, new Date()));
        processes.add(new Process(UUID.randomUUID(), 3, new Date()));

        Mockito.when(processManager.getProcesses()).thenReturn(processes);

        TaskResponse sortedProcesses = processService.getSorted("timestamp");

        Assertions.assertNotNull(sortedProcesses);
        Assertions.assertEquals(3, sortedProcesses.getProcessList().size());
        Assertions.assertEquals(2, sortedProcesses.getProcessList().get(0).getPriority());
        Assertions.assertEquals(1, sortedProcesses.getProcessList().get(1).getPriority());
        Assertions.assertEquals(3, sortedProcesses.getProcessList().get(2).getPriority());
    }

}